# Dandi-DNS

Dynamic IP manager for Gandi

# Installation

Run

```sh
npm install -g dandi-dns
```

# Usage

```sh
dandi-dns ip.json config.json token
```

If nothing is provided, `/etc/dandi-dns/ip.json`, `/etc/dandi-dns/config.json` and `/etc/dandi-dns/token`
are used.

# Configuration:

Dandi-DNS uses three config files:

- `/etc/dandi-dns/ip.json` where the last recorded IP is stored. Nothing to do here, just ensure your user has write access.
- `/etc/dandi-dns/token` Your gandi token, that you can access from https://account.gandi.net/fr/users/<YourUser>/security
- `/etc/dandi-dns/config.json`: A JSON array, as follow:

```json
[
  {
    "domain": "yourdomain.tld",
    "names": [
      {
        "rrset_name": "yoursubdomain",
        "rrset_types": ["A"]
      }
    ]
  }
]
```
