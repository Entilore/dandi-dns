import * as ipManager from "./src/ipManager.js";
import * as gandiManager from "./src/gandiManager.js";

async function updateIfNeeded() {
  const newIp = ipManager.ipUpdate();

  if (!newIp) {
    console.log("IP did not change");
    process.exit();
  }

  console.log("IP did change");

  await gandiManager.updateDomainsIp(newIp);
  ipManager.save(newIp);
}

await updateIfNeeded();
