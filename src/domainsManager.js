import fs from "fs";
import { configFile } from "./config.js";

/**
 * @returns {{domain: String, names: {rrset_name: String, rrset_types: String[]}[]}[]}
 */
function domainsToWatch() {
  return JSON.parse(fs.readFileSync(configFile()));
}

/**
 * @returns {Generator<{rrsetName: String, domain: String, rrsetType: String}>}
 */
export function* domainIterator() {
  for (const { domain, names } of domainsToWatch()) {
    for (const { rrset_name: rrsetName, rrset_types: rrsetTypes } of names) {
      for (const rrsetType of rrsetTypes) {
        yield { domain, rrsetName, rrsetType };
      }
    }
  }
}
