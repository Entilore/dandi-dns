import fs from "fs";

export function ipFile() {
  return process.argv[2] || "/etc/dandi-dns/ip.json";
}

export function configFile() {
  return process.argv[3] || "/etc/dandi-dns/config.json";
}

export function gandiToken() {
  return process.argv[4] || `${fs.readFileSync("/etc/dandi-dns/token")}`.trim();
}
