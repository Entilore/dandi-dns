import fs from "fs";
import { execSync } from "child_process";
import { ipFile } from "./config.js";

function old() {
  try {
    return JSON.parse(fs.readFileSync(ipFile()))?.ip;
  } catch (error) {
    if (!error instanceof SyntaxError) {
      console.error(error);
    }
  }
}

function current() {
  const child = execSync("dig @resolver4.opendns.com myip.opendns.com +short");

  return child.toString().trim();
}

export function ipUpdate() {
  const currentIp = current();
  const oldIp = old();

  if (currentIp !== oldIp) return { newIp: currentIp, oldIp: oldIp };
}

export function save({ newIp }) {
  fs.writeFileSync(ipFile(), JSON.stringify({ ip: newIp }));
}
