import fetch from "node-fetch";
import { gandiToken } from "./config.js";
import { domainIterator } from "./domainsManager.js";

async function call({ domain, rrsetName, rrsetType }, body) {
  const options = {
    method: body ? "PUT" : "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Apikey ${gandiToken()}`,
    },
    body: body ? JSON.stringify(body) : body,
  };

  const response = await fetch(
    `https://api.gandi.net/v5/livedns/domains/${domain}/records/${rrsetName}/${rrsetType}`,
    options
  );

  return response.json();
}

/**
 * @returns {Promise<{rrset_values:String[],any}>}
 */
const get = async (domain) => call(domain);

const set = async (domain, body) => call(domain, body);

function getNextRRSetValues({ oldIp, newIp }, rrsetValues) {
  if (rrsetValues.includes(oldIp)) {
    return rrsetValues.map((value) => (value === oldIp ? newIp : value));
  }
  return [...rrsetValues, newIp];
}

async function updateDomainIp({ oldIp, newIp }, domain) {
  const currentValue = await get(domain);

  if (currentValue.rrset_values.includes(newIp)) {
    return;
  }

  const newRRsetValues = getNextRRSetValues(
    { oldIp, newIp },
    currentValue.rrset_values
  );

  await set(domain, { ...currentValue, rrset_values: newRRsetValues });
}

export async function updateDomainsIp({ oldIp, newIp }) {
  const domains = domainIterator();

  for (const domain of domains) {
    await updateDomainIp({ oldIp, newIp }, domain);
  }
}
